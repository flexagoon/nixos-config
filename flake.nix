{
  description = "flexagoon's NixOS configuration";

  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-unstable;
    home-manager.url = github:nix-community/home-manager;
    #nix-doom-emacs.url = github:vlaci/nix-doom-emacs;
    #nur.url = github:nix-community/NUR;
    kmonad.url = github:kmonad/kmonad?dir=nix;
    utils.url = github:gytis-ivaskevicius/flake-utils-plus;

    #nord-xresources = {
    #  url = github:arcticicestudio/nord-xresources;
    #  flake = false;
    #};
  };

  outputs = inputs@{self, nixpkgs, home-manager, kmonad, utils}: utils.lib.mkFlake {
    inherit self inputs;

    channelsConfig.allowUnfree = true;

    hostDefaults.modules = [{
      nix.generateRegistryFromInputs = true;
      nix.generateNixPathFromInputs = true;
      nix.linkInputs = true;
    }];

    hosts.Laptomatop.modules = [
      ./hosts/Laptomatop
      kmonad.nixosModules.default
      home-manager.nixosModules.home-manager
      {
        home-manager.useGlobalPkgs = true;
        home-manager.useUserPackages = true;
      }
    ];
    #nixosConfigurations.Laptomatop = inputs.nixpkgs.lib.nixosSystem {
    #  system = "x86_64-linux";
    #  modules = [
    #    ./hosts/Laptomatop
    #    inputs.kmonad.nixosModule
    #    inputs.home-manager.nixosModules.home-manager
    #    {
    #      home-manager.useGlobalPkgs = true;
    #      home-manager.useUserPackages = true;
    #      home-manager.users.flexagoon.imports = [ inputs.nix-doom-emacs.hmModule ];
    #    }
    #    { nixpkgs.overlays = [ inputs.nur.overlay ]; }
    #    { nix.nixPath = [ "nixpkgs=${inputs.nixpkgs}" ]; }
    #  ];
    #};
  };
}
