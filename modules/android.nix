{ pkgs, ... }:

let
  androidComposition = pkgs.androidenv.composeAndroidPackages {
    platformVersions = [ "28" "29" "30" "31" ];
    buildToolsVersions = [ "29.0.2" "31.0.0" ];
    #includeEmulator = true;
    #emulatorVersion = "30.9.0";
  };
in 
{
  programs.adb.enable = true;
  users.users.flexagoon.extraGroups = [ "adbusers" ];
  nixpkgs.config.android_sdk.accept_license = true;
  environment.systemPackages = with pkgs; [
    flutter
    dart
    androidComposition.androidsdk
    #android-studio
  ];
  environment.sessionVariables = {
    ANDROID_HOME = "${androidComposition.androidsdk}/libexec/android-sdk";
    ANDROID_SDK_ROOT = "${androidComposition.androidsdk}/libexec/android-sdk";
    GRADLE_OPTS = "-Dorg.gradle.project.android.aapt2FromMavenOverride=${androidComposition.androidsdk}/libexec/android-sdk/build-tools/31.0.0/aapt2";
  };
}
