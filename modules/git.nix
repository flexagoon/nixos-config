{
  programs.git = {
    enable = true;
    config = {
      user.name = "flexagoon";
      user.email = "contact@flexagoon.com";
      user.signingkey = "2868BD4BFFAAB38E";
      init.defaultBranch = "main";
      credential.helper = "store";
      push.default = "current";
    };
  };
}
