{ home-manager, ... }:

{
  home-manager.users.flexagoon.xdg.enable = true;
  home-manager.users.flexagoon.xresources.path = ".config/X11/Xresources";
  environment.sessionVariables = {
    XDG_CACHE_HOME = "$HOME/.cache";
    XDG_CONFIG_HOME = "$HOME/.config";
    XDG_DATA_HOME = "$HOME/.local/share";
    XDG_BIN_HOME = "$HOME/.local/bin";

    HISTFILE = "$XDG_STATE_HOME/history";
    XCOMPOSECACHE = "$XDG_CACHE_HOME/X11/xcompose";
    STACK_ROOT = "$XDG_DATA_HOME/stack";
    GOPATH = "$XDG_DATA_HOME/go";
    MYSQL_HISTFILE = "$XDG_DATA_HOME/mysql_history";
    CUDA_CACHE_PATH = "$XDG_CACHE_HOME/nv";
    SSB_HOME = "$XDG_DATA_HOME/zoom";
    GRADLE_USER_HOME = "$XDG_DATA_HOME/gradle";
    _JAVA_OPTIONS = "-Djava.util.prefs.userRoot=$XDG_CONFIG_HOME/java";
    PUB_CACHE = "$XDG_CACHE_HOME/pub";
    TEXMFHOME = "$XDG_DATA_HOME/texmf";
    TEXMFVAR = "$XDG_CACHE_HOME/texlive/texmf-var";
    GNUPGHOME = "$XDG_DATA_HOME/gnupg";
    KDEHOME = "$XDG_CONFIG_HOME/kde";
    NODE_REPL_HISTORY = "$XDG_DATA_HOME/node_repl_history";
    GTK2_RC_FILES = "$XDG_CONFIG_HOME/gtk-2.0/gtkrc";
    LESSHISTFILE = "$XDG_STATE_HOME/less/history";
  };
}
