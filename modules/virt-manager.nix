{ pkgs, ... }:

{
  virtualisation.libvirtd.enable = true;
  programs.dconf.enable = true;
  users.extraGroups.libvirtd.members = [ "flexagoon" ];
  environment.systemPackages = with pkgs; [ 
    virt-manager 
    qemu
  ];
}
