{
  services.kmonad = {
    enable = true;
    keyboards.laptop = {
      name = "laptop-internal";
      device = "/dev/input/by-id/usb-ASASTeK_COMPUTER_INC._ROG_MacroKey-event-kbd";
      config = (builtins.readFile ./kde.kbd);
    };
  };
}
