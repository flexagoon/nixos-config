{ pkgs, ... }:

{
  programs.neovim = {
    enable = true;
    defaultEditor = true;
    viAlias = true;
    vimAlias = true;
    configure = {
      packages.myVimPackage = with pkgs.vimPlugins; {
        start = [
          palenight-vim
          vim-nix
        ];
      };
      customRC = ''
        set clipboard+=unnamedplus
        set splitbelow
        set splitright
        set laststatus=1
        set number
        set iskeyword-=_
        set iskeyword+=/
        set noshowmode
        set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz
        set mouse=a
        set background=dark
        colorscheme palenight
        set termguicolors
        hi visual cterm=reverse
        noremap <silent> <ESC> :noh<CR>
        
        autocmd BufReadPost *
          \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
          \ |   exe "normal! g`\""
          \ | endif
      '';
    };
  };
}
