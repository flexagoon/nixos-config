{
  nix.settings.auto-optimise-store = true;
  nix.gc = {
    automatic = true;
    dates = "daily";
  };
  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';
}
