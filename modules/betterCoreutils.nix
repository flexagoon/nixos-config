{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    exa
    bat
    bpytop
    archiver
    fd
    prettyping
    ripgrep
  ];
  security.sudo.enable = false;
  security.doas = {
    enable = true;
    extraRules = [{ groups = [ "wheel" ]; persist = true; keepEnv = true; }];
  };
  environment.shellAliases = {
    ls = "exa --icons";
    cat = "bat";
    find = "fd";
    ping = "prettyping";
    grep = "rg";
    sudo = "doas ";
    doas = "doas "; # Makes aliases work under root
    #doasedit = ''function _(){fname="$(echo $1|rev|cut -d/ -f1|rev)" && cp "$1" "/tmp/$fname" && v "/tmp/$fname" && doas cp "/tmp/$fname" "$1" && rm "/tmp/$fname"}; _''; # Like sudoedit, but for doas
    #sudoedit = "doasedit";
  };
}
