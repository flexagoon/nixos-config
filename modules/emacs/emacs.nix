{ home-manager, config, ... }:

{
  home-manager.users.flexagoon = {
    programs.doom-emacs = {
      enable = true;
      doomPrivateDir = ./doom.d;
    };
  };
}
