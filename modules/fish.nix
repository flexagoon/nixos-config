{ pkgs, home-manager, ... }:

{
  programs.fish.enable = true;
  programs.starship.enable = true;
  users.defaultUserShell = pkgs.fish;
  home-manager.users.flexagoon = {
    programs.fish = {
      enable = true;
      functions.fish_greeting = "pfetch";
      interactiveShellInit = ''
        set -g fish_color_normal normal
        set -g fish_color_command green
        set -g fish_color_keyword yellow
        set -g fish_color_quote yellow
        set -g fish_color_error 'red' '--bold'
        set -g fish_color_param normal
        set -g fish_color_option cyan
        set -g fish_color_operator blue
        set -g fish_color_autosuggestion 515772
        set -g fish_color_cancel -r
        set -g fish_color_redirection brblue
        set -g fish_color_end magenta
        set -g fish_color_escape bryellow
      '';
      shellAbbrs = {
        v = "nvim";
      };
      shellAliases = {
        s = "$EDITOR /etc/nixos/hosts/Laptomatop/default.nix";
        i = "$EDITOR /etc/nixos/hosts/Laptomatop/packages.nix";
        sa = "sudo nixos-rebuild switch --flake path:/etc/nixos";
        update = "sudo nix flake update --commit-lock-file /etc/nixos && sudo nixos-rebuild switch --flake path:/etc/nixos";
        rb = "reboot";
        c = "clear";
        x = "exit";
      };
    };
    home.sessionPath = [ "$HOME/.emacs.d/bin" "$HOME/.local/bin" ];
  };
}
