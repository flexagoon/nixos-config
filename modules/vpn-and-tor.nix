{ pkgs, home-manager, ... }:

{
  environment.systemPackages = with pkgs; [ 
    mullvad-vpn 
    #tor-browser-bundle-bin
  ];
  services.mullvad-vpn.enable = true;
  security.wrappers.mullvad-exclude = {
    setuid = true;
    owner = "root";
    group = "root";
    source = "${pkgs.mullvad-vpn}/bin/mullvad-exclude";
  };
  #home-manager.users.flexagoon.xdg.desktopEntries.torbrowser = {
  #  name = "Tor Browser";
  #  exec = "mullvad-exclude tor-browser %U";
  #  icon = "torbrowser";
  #  genericName = "Web Browser";
  #  comment = "Tor Browser Bundle built by torproject.org";
  #  categories = [ "Network" "WebBrowser" "Security" ];
  #};
}
