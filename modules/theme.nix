{ home-manager, nord-xresources, ... }:

{
  home-manager.users.flexagoon = {
    xresources = {
      #extraConfig = builtins.readFile "${nord-xresources}/src/nord"; # TODO use palenight
      properties = {
        "*.alpha" = 1;
        "*.font" = "FiraCode Nerd Font Mono:pixelsize=19:antialias=true:autohint=true";
      };
    };
  };
}
