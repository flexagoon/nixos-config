{ home-manager, ... }:

{
  home-manager.users.flexagoon = {
    services.dunst = {
      enable = true;
      settings = {
        global = {
          font = "FiraCode Nerd Font 10.5";
          horizontal_padding = 8;
          format = "%a | %s: %b";
          max_icon_size = 20;
        };
        urgency_normal = {
          background = "#292d3e";
          foreground = "#a6accd";
          timeout = 5;
        };
        urgency_critical = {
          background = "#f07178";
          foreground = "#a6accd";
        };
        urgency_low = {
          background = "#82aaff";
          foreground = "#a6accd";
        };
      };
    };
  };
}
