{
  hardware.pulseaudio.enable = true;
  hardware.pulseaudio.support32Bit = true;
  users.extraUsers.flexagoon.extraGroups = [ "audio" ];
}
