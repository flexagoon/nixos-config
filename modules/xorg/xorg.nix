{ lib, pkgs, ... }:

{
  services.xserver = {
    enable = true;
    libinput.enable = true;
    layout = "us,ru";
    xkbOptions = "grp:alt_shift_toggle,compose:ralt";
    displayManager.sddm.enable = true;
  };
  console.useXkbConfig = true;
  environment.systemPackages = [ pkgs.xclip ];
  fonts.fonts = with pkgs; [
    fira-code
    fira
    (nerdfonts.override { fonts = [ "FiraCode" ]; })
    comic-relief
    inter
  ];
}
