{
  services.unclutter-xfixes = {
    enable = true;
    timeout = 5;
    extraOptions = [ "exclude-root" ];
  };
}
