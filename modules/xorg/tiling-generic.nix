{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    flameshot
    xkblayout-state
    libnotify
    pavucontrol
  ];
}
