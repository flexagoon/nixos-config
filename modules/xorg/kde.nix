{ pkgs, ... }:

{
  services.xserver.desktopManager.plasma5.enable = true;
  services.xserver.desktopManager.plasma5.runUsingSystemd = true;
  programs.kdeconnect.enable = true;
  environment.systemPackages = with pkgs; [ 
    plasma-browser-integration 
    libsForQt5.kalarm
    papirus-icon-theme
    materia-theme materia-kde-theme
    lightly-qt
    ark
    (sierra-breeze-enhanced.overrideAttrs (oldAttrs: rec {
      postPatch = ''
        substituteInPlace breeze.json \
          --replace "\"blur\": true" "\"blur\": false"
      '';
    }))
  ];
  nixpkgs.config.firefox.enablePlasmaBrowserIntegration = true;
}
