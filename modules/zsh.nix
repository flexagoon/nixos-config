{ pkgs, home-manager, ... }:

{
  programs.zsh.enable = true;
  #users.defaultUserShell = pkgs.zsh;
  home-manager.users.flexagoon = {
    programs.zsh = {
      enable = true;
      dotDir = ".config/zsh";
      enableCompletion = false;
      zplug = {
        enable = true;
        plugins = [
          { name = "romkatv/powerlevel10k"; tags = [ as:theme depth:1 ]; }
          { name = "zsh-users/zsh-autosuggestions"; }
          { name = "marlonrichert/zsh-autocomplete"; tags = [ defer:2 ]; }
          { name = "zdharma/fast-syntax-highlighting"; }
          { name = "ael-code/zsh-colored-man-pages"; }
        ];
      };
      initExtraFirst = ''
        echo
        export PF_INFO="ascii title os uptime shell memory"
        clear
        if [[ -r "''${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-''${(%):-%n}.zsh" ]]; then
          source "''${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-''${(%):-%n}.zsh"
        fi
      '';
      initExtra = ''
        zstyle ':completion:*:paths' path-completion yes
        [[ ! -f ~/.config/zsh/.p10k.zsh ]] || source ~/.config/zsh/.p10k.zsh
        bindkey "''${key[Up]}" up-line-or-search
      '';
      history = {
        size = 10000000;
        save = 10000000;
        # path = "${config.xdg.dataHome}/zsh/history"; TODO use this
        path = ".local/share/zsh/history";
      };
    };

    home.sessionPath = [ "$HOME/.emacs.d/bin" "$HOME/.local/bin" ];
  };
  environment.shellAliases = {
    apply = "exec zsh";
    clear = "clear && echo && pfetch";
    s = "v /etc/nixos/hosts/Laptomatop/default.nix";
    i = "v /etc/nixos/hosts/Laptomatop/packages.nix";
    sa = "sudo nixos-rebuild switch --flake path:/etc/nixos";
    v = "$EDITOR";
    sv = "sudoedit";
    c = "clear";
    update = "sudo nix flake update --commit-lock-file /etc/nixos && sudo nixos-rebuild switch --flake path:/etc/nixos";
    rb = "reboot";
    x = "exit";
    killall = "pkill -KILL";

    nex = "source ~/Documents/Code/lcthw/nex";
  };
}
