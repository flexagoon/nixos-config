{
  networking = {
    #useNetworkd = true;
    useDHCP = false;
    interfaces.enp4s0.useDHCP = true;
    interfaces.wlp3s0.useDHCP = true;
    wireless = {
      enable = true;
      interfaces = [ "wlp3s0" ];
      #interfaces = [ "wg0" ];
      networks = {
        "Zavidovo WiFi" = {};
        "OnePlus 7T Pro" = {
          pskRaw = "a698c16e0142637ba1650f2af7c861802938a518e9acb50f66f038f74e363231";
        };
        "MikroTik Wifi (Fast)" = {
          pskRaw = "f73e5483dc4f1d76272d81d625f28775da454645f220ff97541703840855b443";
        };
        "office.n.school" = {
          pskRaw = "c8c7b85d8409a38933f313a25c289dad3c08d86aff08bf234e42ff8daba7f73f";
        };
      };
    };
  };
}
