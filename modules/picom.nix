{
  services.picom = {
    enable = true;
    experimentalBackends = true;
    backend = "xrender";
    vSync = true;
    settings.unredir-if-possible = true;
  };
}
