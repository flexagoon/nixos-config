{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [
      (modulesPath + "/installer/scan/not-detected.nix")
      ../../modules/xorg/nvidia.nix
    ];

  boot.initrd.availableKernelModules = [ "xhci_pci" "ahci" "usbhid" "sd_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];

  hardware.cpu.intel.updateMicrocode = true;

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/bd8d8859-fa95-410c-a53e-ad736619b142";
      fsType = "ext4";
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/2447-7A5E";
      fsType = "vfat";
    };

  fileSystems."/home" =
    { device = "/dev/disk/by-uuid/1c9cd939-4eda-49a2-a434-983857f5a834";
      fsType = "ext4";
    };

  swapDevices =
    [ { device = "/dev/disk/by-uuid/52a59067-4198-4e4f-9c3f-b07fd377ab00"; }
    ];

  powerManagement.cpuFreqGovernor = lib.mkDefault "ondemand";
  #services.tlp.enable = true;

  hardware.bluetooth.enable = true;
}
