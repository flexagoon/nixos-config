{ pkgs, lib, ... }:

{
  environment.systemPackages = with pkgs; [
    libreoffice
    inkscape
    hugo
    thunderbird
    audacity
    syncthing
    # Looks / Usability
    pfetch
    # Media
    sxiv
    #zathura
    obs-studio
    qbittorrent
    imagemagick
    ffmpeg
    yt-dlp
    # Games
    polymc
    protonup
    # Network
    firefox
    # Social
    tdesktop
    # Work / Produtivity
    texlive.combined.scheme-full
    krita
    kdenlive
    blender
    winbox
    joplin-desktop
    obsidian
    vscodium
    gnupg pinentry
    nextcloud-client
    # Coding
    python3 black         # Python
    nixpkgs-fmt rnix-lsp  # Nix
    # Cartography
    josm
    # Wine
    wineWowPackages.staging
    bottles
    geckodriver
    mono
    # Other
    dex2jar
    mtpfs
    nix-prefetch-scripts
    nix-prefetch-github
  ];
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "steam" "steam-original" "steam-runtime"
    "nvidia-x11" "nvidia-settings"
    "obsidian"
    "winbox"
  ];
}
