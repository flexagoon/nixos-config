# vim:fdm=marker

{ config, pkgs, lib, options, ... }:

{
  networking.hostName = "Laptomatop";

  imports =
    [
      ./hardware-configuration.nix
      ./packages.nix
      ../../modules/nix.nix
      ../../modules/pipewire.nix
      ../../modules/xorg/xorg.nix
      ../../modules/xorg/kde.nix
      ../../modules/xorg/unclutter.nix
      ../../modules/networking.nix
      ../../modules/vpn-and-tor.nix
      ../../modules/keyboard/keyboard.nix
      #../../modules/zsh.nix
      ../../modules/fish.nix
      ../../modules/neovim.nix
      ../../modules/git.nix
      ../../modules/betterCoreutils.nix
      ../../modules/android.nix
      ../../modules/xdg.nix
      ../../modules/theme.nix
      ../../modules/mpv.nix
      ../../modules/virt-manager.nix
      #../../modules/syncthing.nix
    ];

  # Boot and low-level stuff {{{
  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    loader = {
      timeout = 3;
      systemd-boot = {
        enable = true;
        consoleMode = "max";
        configurationLimit = 10;
        editor = false;
      };
      efi.canTouchEfiVariables = true;
    };
  };
  # }}}

  # User config {{{
  users = {
    mutableUsers = false;
    users.flexagoon = {
      isNormalUser = true;
      extraGroups = [ "wheel" "input" "uinput" ];
      hashedPassword = "$6$yKvYSlPRVyG/o$uoKWHTxKXA6EX1eVXu4WmhxMuQ2RLSTBpu09iBVJgyHyho7IQQfwmrGgZiMoGA5KnRMvYFtl1AO1L6kn.B0cd.";
    };
  };
  # }}}

  # Packages that need extra configuration {{{
  programs = {
    steam.enable = true;
    java.enable = true;
    java.package = pkgs.jdk8;
    gnupg.agent.enable = true;
    #gnupg.agent.pinentryFlavor = "curses";
  };
  #}}}

  # Time and location{{{
  time.timeZone = "Europe/Moscow";
  location.provider = "geoclue2";
  # }}}

  services.syncthing.enable = true;

  home-manager.users.flexagoon.systemd.user.startServices = true;

  programs.command-not-found.enable = false;

  system.stateVersion = "21.11";
  home-manager.users.flexagoon.home.stateVersion = "21.11";
}

